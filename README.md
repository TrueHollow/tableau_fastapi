# Simple JSON/XML Web Data Connector (2019.4+)

Based on [example provided by Keshia Rose](https://repl.it/@KeshiaRose/json-xml-wdc)

## Requirements
1. Python version 3.8+

## Installation
1. Install dependencies (`pipenv install`)

## Starting app
1. Run standard command (`uvicorn main:app --host=0.0.0.0 --port=${PORT}`)

## Notes
App will try to get http port number from environment variable `PORT`. If variable isn't set, then app will use port 8000
