from fastapi.testclient import TestClient
from main import app
from pathlib import Path

client = TestClient(app)


def test_index_route():
    response = client.get('/')
    assert response.status_code == 200
    assert response.headers['content-type'].find('text/html') != -1


def test_get_proxy_ok():
    response = client.get('/proxy/https://json-xml-wdc.keshiarose.repl.co/food.json')
    assert response.status_code == 200
    assert response.headers['content-type'].find('application/json') != -1
    json = response.json()
    assert isinstance(json['body'], str)


def test_get_proxy_not_found():
    response = client.get('/proxy/https://json-xml-wdc.keshiarose.repl.co/food1.json')
    assert response.status_code == 200
    assert response.headers['content-type'].find('application/json') != -1
    json: dict = response.json()
    assert json.get('body', None) is None
    assert isinstance(json['error'], str)


def test_get_proxy_timeout():
    response = client.get('/proxy/http://localhost:3001/food.json')
    assert response.status_code == 200
    assert response.headers['content-type'].find('application/json') != -1
    json: dict = response.json()
    assert json.get('body', None) is None
    assert isinstance(json['error'], str)


def test_post_xml_endpoint():
    path_to_xml_file = Path(__file__).parent.parent.joinpath('public', 'orders.xml')
    with open(path_to_xml_file) as f:
        xml_string = f.read()
        data = {'xml': xml_string}
        response = client.post('/xml', data=data)
        assert response.status_code == 200
        assert response.headers['content-type'].find('application/json') != -1


def test_post_xml_invalid_str():
    data = {'xml': 'fooBar'}
    response = client.post('/xml', data=data)
    assert response.status_code == 400
    assert response.headers['content-type'].find('application/json') != -1
