from fastapi import FastAPI, Depends, HTTPException, Form, Request
from uvicorn import run as run_app
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.responses import FileResponse, HTMLResponse
from httpx import AsyncClient, get
from xmltodict import parse as parse_xml_to_dict
import logging
from pathlib import Path

app: FastAPI = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

APP_ROOT = Path(__file__).resolve().parent
INDEX_HTML_PATH = APP_ROOT.joinpath('public', 'index.html')


@app.get("/", response_class=HTMLResponse, name="Index", description="Return index.html page", tags=["html"])
async def read_index():
    return FileResponse(INDEX_HTML_PATH)


async def proxy_request(url: str, req: Request):
    logging.info("Proxy request")
    query = req.url.query
    async with AsyncClient() as client:
        if query:
            url += f"?{query}"
        logging.info(f"Full url: {url}")
        try:
            if req.method == 'GET':
                response = await client.get(url)
            elif req.method == 'POST':
                response = await client.post(url)
            if response.is_error:
                logging.error(response.reason_phrase)
                return {'error': f'Error: {response.reason_phrase}'}
            logging.info("Response resolved")
            content = response.content
            logging.info("Sending result")
            return {'body': content}
        except TimeoutError as err:
            return {'error': f'Timeout error: {err}'}
        except Exception as err:
            logging.error(err)
            return {'error': 'Unexpected error'}


def proxy_request_sync(url: str, req: Request):
    logging.info("Proxy request sync")
    query = req.url.query
    if query:
        url += f"?{query}"
    logging.info(f"Full url: {url}")
    response = get(url)
    logging.info("Response resolved")
    content = response.content
    logging.info("Sending result")
    return {'body': content}


@app.get("/proxy/{url:path}")
async def proxy_get(content: dict = Depends(proxy_request)):
    return content


@app.post("/proxy/{url:path}")
async def proxy_get(content: dict = Depends(proxy_request)):
    return content


@app.post("/xml")
def parse_xml_to_json(*, xml: str = Form(...)):
    try:
        result = parse_xml_to_dict(xml)
        return result
    except Exception as x:
        logging.error(x)
        details = {'error': f'Invalid xml: {x}'}
        raise HTTPException(status_code=400, detail=details)


PUBLIC_DIRECTORY_PATH = APP_ROOT.joinpath('public')
app.mount("/", StaticFiles(directory=PUBLIC_DIRECTORY_PATH), name="public")

if __name__ == "__main__":
    run_app(app, host='0.0.0.0', port=8000)
